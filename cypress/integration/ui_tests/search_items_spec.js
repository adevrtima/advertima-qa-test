describe('Search Items Tests', ()=> {
    beforeEach(() => {
        cy.visit('https://demo.nopcommerce.com/')

        cy.get('.search-box-text').as('searchBox')
        cy.get('.search-box-button').as('searchButton')
        cy.get('.product-title').as('productTitle')
    })

    it('Searches existing item with two criteria', ()=>{
        cy.get('@searchBox').type('apple macbook').should('have.value', 'apple macbook');
        cy.get('@searchButton').click();
        cy.get('@productTitle').first().should('have.text', 'Apple MacBook Pro 13-inch');
    })

    it('Searches existing item with two criteria', ()=>{
        cy.get('@searchBox').type('apple 13-inch').should('have.value', 'apple 13-inch');
        cy.get('@searchButton').click();
        cy.get('@productTitle').first().should('have.text', 'Apple MacBook Pro 13-inch');
    })
    
    it('Searches non-existing item with three criteria', ()=>{
        cy.get('@searchBox').type('iphone 12 pro').should('have.value', 'iphone 12 pro');
        cy.get('@searchButton').click();
        cy.get('.no-result').should('have.text', 'No products were found that matched your criteria.');
    })

    
    it('Searches items with three criteria', ()=>{
        cy.get('@searchBox').type('apple macbook pro').should('have.value', 'apple macbook pro');
        cy.get('@searchButton').click();
        cy.get('@productTitle').first().should('have.text', 'Apple MacBook Pro 13-inch');
    })


})
