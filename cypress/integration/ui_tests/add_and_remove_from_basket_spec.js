describe('Add Items To Basket Tests', ()=> {
    beforeEach(() => {
        cy.visit('https://demo.nopcommerce.com/cell-phones')
        
        cy.get('.product-box-add-to-cart-button').as('addToCart')
        cy.get('.cart-qty').as('amountOfItemsInCart')
    })

    it('Aliasing', ()=>{

    })

    it('Adds item to the basket', () =>{
        cy.get('@addToCart').first().click();
        cy.get('.bar-notification.success').should('be.visible').and('have.text', 'The product has been added to your shopping cart');
        cy.get('@amountOfItemsInCart').should('have.text', '(1)');
    })

    
    it('Removes item from the basket', () =>{
        cy.get('@addToCart').first().click();
        cy.get('.bar-notification.success').should('be.visible').and('have.text', 'The product has been added to your shopping cart');
        cy.get('@amountOfItemsInCart').should('have.text', '(1)').click();
        cy.get('.remove-btn').click();
        cy.get('@amountOfItemsInCart').should('have.text', '(0)');
        cy.get('.no-data').should('have.text', '\nYour Shopping Cart is empty!\n');
    })
})