describe('Test adding new pet', () => {
    
    it('adds new pet through /pet endpoint', () => {
        cy.request({
            method: 'POST',
            url:'https://petstore.swagger.io/v2/pet',
            body: {
                "id": 0,
                "category": {
                  "id": 0,
                  "name": "string"
                },
                "name": "doggie",
                "photoUrls": [
                  "string"
                ],
                "tags": [
                  {
                    "id": 0,
                    "name": "string"
                  }
                ],
                "status": "available"
            }
            }).its('status')
        .should('equal', 200);
    })

        
    it('adds new pet through /pet endpoint with invalid status data', () => {
      cy.request({
          method: 'POST',
          url:'https://petstore.swagger.io/v2/pet',
          body: {
              "id": 0,
              "category": {
                "id": 0,
                "name": "string"
              },
              "name": "doggie",
              "photoUrls": [
                "string"
              ],
              "tags": [
                {
                  "id": 0,
                  "name": "string"
                }
              ],
              "status": "unavailable"
          }
          }).its('status')
      .should('equal', 405);
  })

  it('adds new pet through /pet endpoint with missing payload', () => {
    cy.request({
        method: 'POST',
        url:'https://petstore.swagger.io/v2/pet',
        body:{}
        }).its('status')
    .should('equal', 405);
})
})