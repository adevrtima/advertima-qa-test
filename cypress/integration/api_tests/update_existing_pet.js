describe('Test updating existing pet', () => {
    
    it('updates existing pet through /pet endpoint', () => {
        cy.request({
            method: 'PUT',
            url:'https://petstore.swagger.io/v2/pet',
            body: {
                "id": 0,
                "category": {
                  "id": 0,
                  "name": "string"
                },
                "name": "doggie",
                "photoUrls": [
                  "string"
                ],
                "tags": [
                  {
                    "id": 0,
                    "name": "string"
                  }
                ],
                "status": "pending"
            }
            }).its('status')
        .should('equal', 200);
    })

    it('invalid id supplied', () => {
        cy.request({
            method: 'PUT',
            url:'https://petstore.swagger.io/v2/pet',
            body: {
              "id": 3.14,
              "category": {
                "id": 34,
                "name": "string"
              },
              "name": "dadvng	uifhesrfbuwe4fexhuibedwe",
              "photoUrls": [
                "string"
              ],
              "tags": [
                {
                  "id": 0,
                  "name": "string"
                }
              ],
              "status": "unavailable"
            }
            }).its('status')
        .should('equal', 400);
    })
})