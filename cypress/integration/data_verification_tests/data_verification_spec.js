describe('JSON data verification', ()=> {
    it('validates gender data (gender can be male or female)validates age data (age should be a number)', ()=>{
            cy.fixture('test_data').
            then(test_data => {
                let profiles = test_data.data.profiles;
                let i = profiles.length - 1;
                let j;
                
                for(j = 0; j < i; j++){
                    expect(profiles[j].gender).to.be.oneOf(['male','female']);
                }
            })
    })

    it('validates age data (age should be a number)', ()=>{
        cy.fixture('test_data').
        then(test_data => {
            let profiles = test_data.data.profiles;
            let i = profiles.length - 1;
            let j;
            for(j = 0; j < i; j++){
                assert.isNumber(profiles[j].age);
            }
        })
    })

    it('extracts men age and exports it to extracted_data.json', ()=>{
        cy.fixture('test_data').
        then(test_data => {
            let profiles = test_data.data.profiles;
            let i = profiles.length - 1;
            let j;
            let list = [];
            for(j = 0; j < i; j++){
                if(profiles[j].gender == 'male'){
                    list.push(profiles[j].age);
                }
            }
            cy.writeFile('extracted_data.json', list);

        })
    })
})