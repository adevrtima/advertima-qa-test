To see test reports, visit: https://dashboard.cypress.io/projects/e3q7k2/runs

To run dockerized tests, in the terminal:
1. Build the image: `docker build -t e2e-cypress:latest .`
2. Run the image: `docker run -it e2e-cypress:latest`
