FROM cypress/browsers:latest
WORKDIR /advertima/e2e-cypress
COPY . .
CMD npx cypress run